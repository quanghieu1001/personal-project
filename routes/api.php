<?php

use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Blog\BlogCommentsController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [LoginController::class, 'authenticate']);

Route::post('find-phone-number', [Controller::class, 'findPhoneNumber']);
Route::post('find-url-in-json', [Controller::class, 'findUrlInJson']);
Route::post('check-is-email', [Controller::class, 'checkIsEmail']);

//blog comments route
Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/blog/comments', [BlogCommentsController::class, 'index']);
    Route::post('/blog/comments', [BlogCommentsController::class, 'store']);
    Route::get('/blog/comments/{comment}', [BlogCommentsController::class,'show']);
    Route::put('/blog/comments/{comment}', [BlogCommentsController::class, 'update']);
    Route::delete('/blog/comments/{comment}', [BlogCommentsController::class, 'destroy']);
});
