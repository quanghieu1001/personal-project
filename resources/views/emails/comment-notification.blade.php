<!DOCTYPE html>
<html lang="vn">
<head>
    <title>New comment from {{$username}}</title>
</head>
<body>
<div style="margin-top:30px;text-align: center">
    <h1 style="text-align: center">New comment from {{$username}}</h1>
    <p style="text-align: center">Comment: {{$comment}}</p>
    <a href="{{$blogUrl}}">Link blog</a>
</div>
</body>
</html>
