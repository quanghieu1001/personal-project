<?php

namespace App\Repositories;

use App\Events\CommentPosted;
use App\Interfaces\BlogCommentRepositoryInterface;
use App\Models\BlogComment;
use App\Models\BlogPost;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Mail;

class BlogCommentsRepository implements BlogCommentRepositoryInterface
{
    private BlogComment $blogComment;
    public function __construct(BlogComment $blogComment)
    {
        $this->blogComment = $blogComment;
    }
    public function getBlogComments(): array
    {
        return $this->blogComment->all()->toArray();
    }

    public function getBlogComment($id): array
    {
        return $this->blogComment->find($id)->toArray();
    }

    public function createBlogComment($data): Model|BlogComment
    {
        $comment = $this->blogComment->make($data->all());
        $comment['user_id'] = Auth::user()->id;
        $comment['like'] = 0;
        $comment['dislike'] = 0;
        $comment->save();

        $blog = BlogPost::whereId($data->get('blog_post_id'))
            ->first()->toArray();

        $sendTo = !empty($blog['created_by']['username'])
            ? $blog['created_by']['username']
            : env('ADMINISTRATOR');
        $blogUrl = config('eup.blog_url').$blog['slug'];

        event(new CommentPosted(Auth::user()->name, $data->get('content'),$blogUrl,$sendTo));

        return $comment;
    }

    public function updateBlogComment($id, $data): array
    {
        $comment = BlogComment::find($id);
        $comment->update($data->all());
        return $comment;
    }

    public function deleteBlogComment($id): array
    {
        $comment = BlogComment::find($id);
        $comment->delete();
        return $comment;
    }
}
