<?php

namespace App\Repositories;

use App\Http\Requests\LoginFormRequest;
use App\Interfaces\LoginRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class LoginRepository implements LoginRepositoryInterface
{
    public function authenticate(LoginFormRequest $request): JsonResponse
    {
        $user = Auth::attempt([
            'email' => strtolower($request->get('email')),
            'password' => $request->get('password')
        ]);

        if (!$user) {
            return response()->json([
                'error' => 'Incorrect username or password'
            ], 401);
        }

        return response()->json([
            'token' => $request->user()->createToken('user-token')->plainTextToken,
            'user' => $request->user()
        ]);
    }
}
