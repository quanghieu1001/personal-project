<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CommentPosted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public string $userName;
    public string $comment;
    public string $blogUrl;
    public string $sendTo;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userName, $comment, $blogUrl, $sendTo)
    {
        $this->userName = $userName;
        $this->comment = $comment;
        $this->blogUrl = $blogUrl;
        $this->sendTo = $sendTo;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn(): Channel|array
    {
        return new Channel('comments');
    }
}
