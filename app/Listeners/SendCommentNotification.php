<?php

namespace App\Listeners;

use App\Events\CommentPosted;
use App\Mail\CommentNotification;
use Exception;
use Illuminate\Support\Facades\Mail;

class SendCommentNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param CommentPosted $event
     * @return void
     */
    public function handle(CommentPosted $event): void
    {
        $userName = $event->userName;
        $comment = $event->comment;
        $blogUrl = $event->blogUrl;
        $sendTo = $event->sendTo;

        try {
            Mail::to($sendTo)
                ->send(new CommentNotification($userName, $comment, $blogUrl, $sendTo));
        } catch (Exception $e) {
            echo "An error occurred: " . $e->getMessage();
        }
    }
}
