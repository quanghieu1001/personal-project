<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\adminUser
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string|null $avatar
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|adminUser newModelQuery()
 * @method static Builder|adminUser newQuery()
 * @method static Builder|adminUser query()
 * @method static Builder|adminUser whereAvatar($value)
 * @method static Builder|adminUser whereCreatedAt($value)
 * @method static Builder|adminUser whereId($value)
 * @method static Builder|adminUser whereName($value)
 * @method static Builder|adminUser wherePassword($value)
 * @method static Builder|adminUser whereRememberToken($value)
 * @method static Builder|adminUser whereUpdatedAt($value)
 * @method static Builder|adminUser whereUsername($value)
 * @mixin \Eloquent
 */
class AdminUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'username',
        'password',
        'name',
        'avatar',
        'remember_token'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

}
