<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\BlogPost
 *
 * @property int $id
 * @property int $blog_category_id
 * @property string $title
 * @property string $slug
 * @property string $thumbnail
 * @property string $short_description
 * @property string $content
 * @property int $user_id
 * @property int $status
 * @property int $comment_count
 * @property string $language
 * @property int $view
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $publish_at
 * @property string|null $deleted_at
 * @property string|null $key_seo
 * @method static Builder|BlogPost newModelQuery()
 * @method static Builder|BlogPost newQuery()
 * @method static Builder|BlogPost query()
 * @method static Builder|BlogPost whereBlogCategoryId($value)
 * @method static Builder|BlogPost whereCommentCount($value)
 * @method static Builder|BlogPost whereContent($value)
 * @method static Builder|BlogPost whereCreatedAt($value)
 * @method static Builder|BlogPost whereDeletedAt($value)
 * @method static Builder|BlogPost whereId($value)
 * @method static Builder|BlogPost whereKeySeo($value)
 * @method static Builder|BlogPost whereLanguage($value)
 * @method static Builder|BlogPost wherePublishAt($value)
 * @method static Builder|BlogPost whereShortDescription($value)
 * @method static Builder|BlogPost whereSlug($value)
 * @method static Builder|BlogPost whereStatus($value)
 * @method static Builder|BlogPost whereThumbnail($value)
 * @method static Builder|BlogPost whereTitle($value)
 * @method static Builder|BlogPost whereUpdatedAt($value)
 * @method static Builder|BlogPost whereUserId($value)
 * @method static Builder|BlogPost whereView($value)
 * @mixin \Eloquent
 */
class BlogPost extends Model
{
    use HasFactory;

    protected $appends = [
        'created_by'
    ];

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(AdminUser::class, 'user_id');
    }

    public function getCreatedByAttribute(): Model|BelongsTo|null
    {
        return $this->createdBy()->first();
    }
}
