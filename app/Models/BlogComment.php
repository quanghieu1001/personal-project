<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\BlogComment
 *
 * @property int $id
 * @property int $blog_post_id
 * @property int $user_id
 * @property string $content
 * @property int $status
 * @property int $like
 * @property int $dislike
 * @property int $parent_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|BlogComment newModelQuery()
 * @method static Builder|BlogComment newQuery()
 * @method static Builder|BlogComment query()
 * @method static Builder|BlogComment whereBlogPostId($value)
 * @method static Builder|BlogComment whereContent($value)
 * @method static Builder|BlogComment whereCreatedAt($value)
 * @method static Builder|BlogComment whereDislike($value)
 * @method static Builder|BlogComment whereId($value)
 * @method static Builder|BlogComment whereLike($value)
 * @method static Builder|BlogComment whereParentId($value)
 * @method static Builder|BlogComment whereStatus($value)
 * @method static Builder|BlogComment whereUpdatedAt($value)
 * @method static Builder|BlogComment whereUserId($value)
 * @mixin \Eloquent
 */
class BlogComment extends Model
{
    use HasFactory;

    protected $fillable = [
        'blog_post_id',
        'user_id',
        'content',
        'status',
        'like',
        'dislike',
        'parent_id',
    ];
}
