<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateBlogCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'blog_post_id' => 'required|integer',
            'content' => 'required|string',
//            'status' => 'required|integer',
//            'like' => 'required|integer',
//            'dislike' => 'required|integer',
            'parent_id' => 'required|integer',
        ];
    }
}
