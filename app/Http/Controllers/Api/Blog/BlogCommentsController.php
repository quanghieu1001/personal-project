<?php

namespace App\Http\Controllers\Api\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBlogCommentRequest;
use App\Http\Requests\UpdateBlogCommentRequest;
use App\Interfaces\BlogCommentRepositoryInterface;
use App\Models\BlogComment;
use Illuminate\Http\JsonResponse;

class BlogCommentsController extends Controller
{
    private BlogCommentRepositoryInterface $blogCommentRepository;

    public function __construct(BlogCommentRepositoryInterface $blogCommentRepository)
    {
         $this->blogCommentRepository = $blogCommentRepository;
    }
    public function index(): JsonResponse
    {
        return response()->json($this->blogCommentRepository->getBlogComments());
    }
    public function store(CreateBlogCommentRequest $request): JsonResponse
    {
        return response()->json($this->blogCommentRepository->createBlogComment($request));
    }

    public function show(BlogComment $comment): JsonResponse
    {
        return response()->json($this->blogCommentRepository->getBlogComment($comment));
    }

    public function update(BlogComment $comment, UpdateBlogCommentRequest $request): JsonResponse
    {
        $comment->update($request->all());
        return response()->json($comment);
    }

    public function destroy(BlogComment $comment): JsonResponse
    {
        $comment->delete();
        return response()->json($comment);
    }
}
