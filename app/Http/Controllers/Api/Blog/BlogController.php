<?php

namespace App\Http\Controllers\Api\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class BlogController extends Controller
{
    public function index (): JsonResponse
    {
        return response()->json();
    }
}
