<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginFormRequest;
use App\Interfaces\LoginRepositoryInterface;
use Illuminate\Http\JsonResponse;

class LoginController extends Controller
{
    private LoginRepositoryInterface $loginRepository;
    public function __construct(LoginRepositoryInterface $loginRepository)
    {
        $this->loginRepository = $loginRepository;
    }
    /**
     * @param LoginFormRequest $request
     * @return JsonResponse
     */
    public function authenticate(LoginFormRequest $request): JsonResponse
    {
        return response()->json($this->loginRepository->authenticate($request));
    }
}
