<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function findPhoneNumber(Request $request): JsonResponse
    {
        if (preg_match('/\b\d{3}[-.\s]?\d{3}[-.\s]?\d{4}\b/', $request->input('text'))) {
            return response()->json(['result' => 'Phone number found']);
        } else {
            return response()->json(['result' => 'No phone number found']);
        }
    }

    public function findUrlInJson(Request $request): JsonResponse
    {
        $input = $request->input('json');
        $data = json_decode($input);

        foreach ($data as $value) {
            if (filter_var($value, FILTER_VALIDATE_URL)) {
                return response()->json(['result' => 'URL found']);
            }
        }

        return response()->json(['result' => 'No URL found']);
    }

    public function checkIsEmail(Request $request): JsonResponse
    {
        $input = $request->input('text');

        if (filter_var($input, FILTER_VALIDATE_EMAIL)) {
            return response()->json(['result' => 'It is a valid email address']);
        } else {
            return response()->json(['result' => 'It is not a valid email address']);
        }
    }
}
