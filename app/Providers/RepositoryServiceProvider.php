<?php

namespace App\Providers;

use App\Interfaces\BlogCommentRepositoryInterface;
use App\Interfaces\LoginRepositoryInterface;
use App\Repositories\BlogCommentsRepository;
use App\Repositories\LoginRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(BlogCommentRepositoryInterface::class, BlogCommentsRepository::class);
        $this->app->bind(LoginRepositoryInterface::class, LoginRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
