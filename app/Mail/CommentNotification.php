<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class CommentNotification extends Mailable
{
    use Queueable, SerializesModels;
    public string $userName;
    public string $comment;
    public string $blogUrl;
    public string $sendTo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userName, $comment, $blogUrl, $sendTo )
    {
        $this->userName = $userName;
        $this->comment = $comment;
        $this->blogUrl = $blogUrl;
        $this->sendTo = $sendTo;
    }

    /**
     * Get the message envelope.
     *
     * @return Envelope
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'New Comment Notification',
        );
    }

    /**
     * Get the message content definition.
     *
     * @return Content
     */
    public function content(): Content
    {
        return new Content(
            markdown: 'emails.comment-notification',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array
     */
    public function attachments(): array
    {
        return [];
    }

    public function build(): CommentNotification
    {
        return $this->with([
                'username' => $this->userName,
                'comment' => $this->comment,
                'blogUrl' => $this->blogUrl,
            ]);
    }

}
