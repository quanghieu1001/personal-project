<?php

namespace App\Interfaces;

use App\Http\Requests\LoginFormRequest;

interface LoginRepositoryInterface {
    public function authenticate(LoginFormRequest $request);
}
