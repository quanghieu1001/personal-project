<?php

namespace App\Interfaces;

use App\Http\Requests\CreateBlogCommentRequest;
use App\Http\Requests\UpdateBlogCommentRequest;

interface BlogCommentRepositoryInterface
{
    public function getBlogComments();
    public function getBlogComment($id);
    public function createBlogComment(CreateBlogCommentRequest $data);
    public function updateBlogComment($id, UpdateBlogCommentRequest $data);
    public function deleteBlogComment($id);
}
